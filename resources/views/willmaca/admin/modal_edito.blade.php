<div class="modal fade" role="dialog" tabindex="-1" id="modal_edito">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><strong>Actualizar Producto:</strong></h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p class="text-capitalize"><strong>Formulario para actualizar producto:</strong></p><br><br>
                <form class="form-control">
                    <input class="form-control" type="hidden" id="hidden_id_edit">
                    <div>
                        <label class="form-label form-text">
                            <strong>Nombre:&nbsp;</strong>
                        </label>
                        <input class="form-control form-control-lg" type="text" id="nombre_producto" name="nombre" placeholder="Nombre Actual Del Producto">
                    </div>
                    <div>
                        <label class="form-label form-text">
                            <strong>Precio:&nbsp;</strong>
                        </label>
                        <input class="form-control form-control-lg" type="text" id="precio_producto" name="precio" placeholder="Precio Actual Del Producto">
                    </div>
                    <div>
                        <label class="form-label form-text">
                            <strong>Descripción:&nbsp;</strong>
                        </label>
                        <textarea class="form-control form-control-lg" id="descripcion_producto" name="descripcion" placeholder="Descripción Del Producto"></textarea>
                    </div>
                    <input class="form-control" type="hidden" id="categoria">
{{--                     <div class="col-md-10 offset-md-1">
                        <select class="form-select form-select-lg" data-bs-toggle="tooltip" data-bss-tooltip="" data-bs-placement="right" name="categoria" title="Categoria">
                            <optgroup label="Selecciona una categoria"></optgroup>
                                @foreach($categoria as $category)
                                    <option value="{{ $category->nombre }}">{{ $category->nombre }}</option>
                            @endforeach
                        </select>
                    </div>           --}}
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" id="cerrar" type="button" data-bs-dismiss="modal"><strong>Cerrar</strong></button>
                <button class="btn btn-success" id="btn_actualizar" type="button" data-bs-dismiss="modal" data-bs-target="#modal_confirm_update" data-bs-toggle="modal"><strong>Actualizar</strong></button>
            </div>
        </div>
    </div>
</div>