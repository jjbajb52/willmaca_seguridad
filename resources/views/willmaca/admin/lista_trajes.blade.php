<!DOCTYPE html>
<html lang="es">

    @include('willmaca.admin.head_admin')

<body>
    <!-- Start: sticky dark top nav with dropdown -->

    @include('willmaca.admin.menu_admin')

    </div><button class="btn btn-primary" data-bs-toggle="modal" data-bss-tooltip="" data-bs-placement="bottom" type="button" style="background: var(--bs-success);" title="Añadir Usuario" data-bs-target="#modal_register_product"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-person-plus-fill fs-4 text-center">
        <path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
        <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"></path>
    </svg>&nbsp;Añadir<br></button>
    <div class="modal fade text-center" role="dialog" tabindex="-1" id="modal_register_product">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Registro de Productos</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>The content of your modal.</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal" style="background: var(--bs-red);color: var(--bs-white);">Cerrar</button><button class="btn btn-primary" type="button">Guardar</button></div>
            </div>
        </div>
    </div>


    <div class="container mt-5">
        <div class="row">
            <div class="col" style="max-height: 100%;">
                <div class="table-responsive">
                    <table class="table" id="mi_tabla">
                        <thead>
                            <tr style="min-width: auto;max-width: 100%;">
                                <th>Nombre</th>
                                <th>Precio</th>
                                {{-- <th>Categoría</th> --}}
                                <th>Descripción</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
{{--                             @foreach($producto as $product)
                            <tr style="min-width: auto;max-width: 100%;">
                                <td>{{ $product->nombre }}</td>
                                <td>{{ $product->precio }}</td>
                                <td>{{ $product->categoria }}</td>
                                <td>{{ $product->descripcion }}</td>
                                <th>Acción</th>
                            </tr>
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

        {{-- Modales --}}

    {{-- Modal Edit --}}

    @include('willmaca.admin.modal_edito')

    {{-- Modal Delete --}}

    @include('willmaca.admin.modal_delete')

    @include('willmaca.admin.admin_script')

    <script>
        $(document).ready(function () {
            $('#mi_tabla').DataTable({
                processing: true,
                /* serverSide: true, */
                "lengthMenu": [ 5, 10, 15, 20, 25, 50, 75, 100, 150, 200, 250, 500, 1000 ],
                /* "ColumnDefs":[{
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button id='btn_edit' class='btn btn-primary' type='button' data-bs-target='#modal_edit' data-bs-toggle='modal'>Editar&nbsp;<svg xmlns='http://www.w3.org/2000/svg' width='1em' height='1em' viewBox='0 0 24 24' stroke-width='2' stroke='currentColor' fill='none' stroke-linecap='round' stroke-linejoin='round' class='icon icon-tabler icon-tabler-edit'><path stroke='none' d='M0 0h24v24H0z' fill='none'></path><path d='M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3'></path><path d='M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3'></path><line x1='16' y1='5' x2='19' y2='8'></line></svg></button>",
                }], */
                language: {
                    search: "Buscar: ",
                    info: "Cantidad de Registros _START_ de _END_ en _TOTAL_ mostrados",
                    lengthMenu:    "Cantidad _MENU_ Registros",
                    infoEmpty:      "No hay registros",
                    infoFiltered:   "(Filtros de _MAX_ total)",
                    infoPostFix:    "",
                    loadingRecords: "Cargando Datos...",
                    zeroRecords:    "No hay Datos",
                    emptyTable:     "Tabla Vacia",
                    paginate: {
                        first: "Primero",
                        previous: "Primera Pagina",
                        next: "Siguiente Pagina >>",
                        last: "Ultima"
                    }
                },
                ajax: "{{ route('willmaca.listado_trajes') }}",
                columns: [
                    /* {data: 'id', name: 'Id' }, */
                    {data: 'nombre', name: 'Nombre' },
                    {data: 'precio', name: 'Precio' },
                    {data: 'descripcion', name: 'Descripcion' },
                    {data: 'action', name: 'action', orderable: false, searchable: false },
                ],
            });    
        });

</script>

<script src="{{ asset('assets/js/script/script.js') }}"></script>

</body>
</html>