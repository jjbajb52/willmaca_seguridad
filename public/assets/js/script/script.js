$(document).on("click", ".editable", function(){
    let id = $(this).attr('id');
    alert(id);
    
    $.ajax({
        url: "http://localhost/maru/proyecto/public/admin/edit/"+id+"/",
        type: "GET",
        dataType: "json",
        success: function(datos){
            $("#nombre_producto").val(datos.resultado.nombre);
            $("#precio_producto").val(datos.resultado.precio);
            $("#descripcion_producto").val(datos.resultado.descripcion);
            $("#hidden_id_edit").val(datos.resultado.id);
            $("#categoria").val(datos.resultado.categoria);
        },
    });
});

$(document).on("click", "#btn_actualizar", function(event){
    alert("anotado");
    event.preventDefault();
    let id = $("#hidden_id_edit").val();
    let nombre = $("#nombre_producto").val();
    let precio = $("#precio_producto").val();
    let descripcion = $("#descripcion_producto").val();
    let categoria = $("#categoria").val();
    console.log(id, nombre, precio, descripcion, categoria);

    $.ajax({
        url: "http://localhost/maru/proyecto/public/admin/update/",
        type: "GET",
        dataType: "json",
        data: {
            id: id,
            nombre: nombre,
            precio: precio,
            descripcion: descripcion,
            categoria: categoria,
        },
        success: function(datos){
            alert("Actualizacion realizada conexito");
        },
    });
});

$(document).on("click", ".delete", function(){
    let id = $(this).attr('id');
    alert(id);
    console.log(id);
});

$(document).on("click", "#cerrar", function(){
    $("#nombre_producto").val('');
    $("#precio_producto").val(''),
    $("#descripcion_producto").val('');
});

$(document).on("click", ".btn-close", function(){
    $("#nombre_producto").val('');
    $("#precio_producto").val(''),
    $("#descripcion_producto").val('');
});
