<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categoria\Categoria;
use App\Models\Producto\Producto;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Session;
use Datatables;
use Illuminate\Http\Response;
use Yajra\DataTables\Contracts\DataTableScope;

class AdminController extends Controller
{
    public function Categorias()
    {
        
            $categoria = Categoria::all();
            return view('willmaca.admin.formulario_productos', compact('categoria'));
        
    }

    public function cargaTotal()
    {
        return Producto::all();
    }

    public function registro(Request $request)
    {
        $request->validate([
            'nombre'        => ['required', 'max:255'],
            'precio'        => ['required', 'numeric','max:255'],
            'descripción'   => ['required', 'min:50'],
            'file'          => ['required'],
        ]);

        if($request->hasfile('file'))
        {
            $categoria = $request->input('categoria');
            switch($categoria)
            {
                case "Botas":
                    $destino = 'public/img/botas';
                    $file    = $request->file('file');
                    $nombre  = $file->getClientOriginalName();
                    $path    = $file->storeAs($destino, $nombre);
                    $ruta    = 'storage/img/botas/'.$nombre;
                    $producto = new Producto();
                    $producto->nombre    = $request->input('nombre');
                    $producto->precio    = $request->input('precio')."$";
                    $producto->categoria = $request->input('categoria');
                    $producto->ruta      = $path;
                    $producto->imagen    = $ruta;
                    $producto->descripcion = $request->input('descripción');
                    $producto->save();
                    break;

                case "Tapa Bocas":
                    $destino = 'public/img/tapa_bocas';
                    $file    = $request->file('file');
                    $nombre  = $file->getClientOriginalName();
                    $path    = $file->storeAs($destino, $nombre);
                    $ruta    = 'storage/img/tapa_bocas/'.$nombre;
                    $producto = new Producto();
                    $producto->nombre    = $request->input('nombre');
                    $producto->precio    = $request->input('precio')."$";
                    $producto->categoria = $request->input('categoria');
                    $producto->ruta      = $path;
                    $producto->imagen    = $ruta;
                    $producto->descripcion = $request->input('descripción');
                    $producto->save();
                    break;

                    case "Lentes":
                        $destino = 'public/img/lentes';
                        $file    = $request->file('file');
                        $nombre  = $file->getClientOriginalName();
                        $path    = $file->storeAs($destino, $nombre);
                        $ruta    = 'storage/img/lentes/'.$nombre;
                        $producto = new Producto();
                        $producto->nombre    = $request->input('nombre');
                        $producto->precio    = $request->input('precio')."$";
                        $producto->categoria = $request->input('categoria');
                        $producto->ruta      = $path;
                        $producto->imagen    = $ruta;
                        $producto->descripcion = $request->input('descripción');
                        $producto->save();
                        break;

                        case "Trajes":
                            $destino = 'public/img/trajes';
                            $file    = $request->file('file');
                            $nombre  = $file->getClientOriginalName();
                            $path    = $file->storeAs($destino, $nombre);
                            $ruta    = 'storage/img/trajes/'.$nombre;
                            $producto = new Producto();
                            $producto->nombre    = $request->input('nombre');
                            $producto->precio    = $request->input('precio')."$";
                            $producto->categoria = $request->input('categoria');
                            $producto->ruta      = $path;
                            $producto->imagen    = $ruta;
                            $producto->descripcion = $request->input('descripción');
                            $producto->save();
                            break;

                            case "Guantes":
                                $destino = 'public/img/guantes';
                                $file    = $request->file('file');
                                $nombre  = $file->getClientOriginalName();
                                $path    = $file->storeAs($destino, $nombre);
                                $ruta    = 'storage/img/guantes/'.$nombre;
                                $producto = new Producto();
                                $producto->nombre    = $request->input('nombre');
                                $producto->precio    = $request->input('precio')."$";
                                $producto->categoria = $request->input('categoria');
                                $producto->ruta      = $path;
                                $producto->imagen    = $ruta;
                                $producto->descripcion = $request->input('descripción');
                                $producto->save();
                                break;
                        
                        default:
                                echo 'categoria no elegida';
                                break;
            }
        }
        $mensaje = " Mensaje:  Producto $producto->nombre Registrado con ¡EXITO!";

        $request->session()->flash('mensaje', $mensaje);

        return redirect('/admin/formulario_productos');

    }

    public function indexListaBotas(Request $request)
    {
        if($request->ajax())
        {
            $producto = Producto::where('categoria', 'Botas')->get();
            return Datatables::of($producto)->addIndexColumn()
            ->addColumn('action', function($producto){
                $button = '<button id="'.$producto->id.'" class="btn btn-success editable" type="button" data-bs-target="#modal_edito" data-bs-toggle="modal">Editar&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path><line x1="16" y1="5" x2="19" y2="8"></line></svg></button>';
                $button.= '<button id="'.$producto->id.'" class="btn btn-danger delete" type="button" style="background: var(--bs-danger);" data-bs-target="#modal_delete" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-trash-fill fs-4 text-center"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path></svg>&nbsp;Eliminar</button>';
                return $button;
            })
            ->make(true);
        }
        
        return view('willmaca.admin.lista_botas');
        
    }

    public function listaTapaBocas(Request $request)
    {
        if($request->ajax())
        {
            $producto = Producto::where('categoria', 'Tapa Bocas')->get();
            return Datatables::of($producto)->addIndexColumn()
            ->addColumn('action', function($producto){
                $button = '<button id="'.$producto->id.'" class="btn btn-success editable" type="button" data-bs-target="#modal_edito" data-bs-toggle="modal">Editar&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path><line x1="16" y1="5" x2="19" y2="8"></line></svg></button>';
                $button.= '<button id="'.$producto->id.'" class="btn btn-danger delete" type="button" style="background: var(--bs-danger);" data-bs-target="#modal_delete" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-trash-fill fs-4 text-center"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path></svg>&nbsp;Eliminar</button>';
                return $button;
            })
            ->make(true);
        }
        
        return view('willmaca.admin.lista_tapa_bocas');
        
    }

    public function listaTrajes(Request $request)
    {
        if($request->ajax())
        {
            $producto = Producto::where('categoria', 'Trajes')->get();
            return Datatables::of($producto)->addIndexColumn()
            ->addColumn('action', function($producto){
                $button = '<button id="'.$producto->id.'" class="btn btn-success editable" type="button" data-bs-target="#modal_edito" data-bs-toggle="modal">Editar&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path><line x1="16" y1="5" x2="19" y2="8"></line></svg></button>';
                $button.= '<button id="'.$producto->id.'" class="btn btn-danger delete" type="button" style="background: var(--bs-danger);" data-bs-target="#modal_delete" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-trash-fill fs-4 text-center"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path></svg>&nbsp;Eliminar</button>';
                return $button;
            })
            ->make(true);
        }
        
        return view('willmaca.admin.lista_trajes');
        
    }

    public function listaBotas(Request $request)
    {
        if($request->ajax())
            {
                $producto = Producto::where('categoria', 'Botas')->get();
                return Datatables::of($producto)->addIndexColumn()
                ->addColumn('action', function($producto){
                    $button = '<button id="'.$producto->id.'" class="btn btn-success editable" type="button" data-bs-target="#modal_edito" data-bs-toggle="modal">Editar&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path><line x1="16" y1="5" x2="19" y2="8"></line></svg></button>';
                    $button.= '<button id="'.$producto->id.'" class="btn btn-danger delete" type="button" style="background: var(--bs-danger);" data-bs-target="#modal_delete" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-trash-fill fs-4 text-center"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path></svg>&nbsp;Eliminar</button>';
                    return $button;
                })
                ->make(true);
            }
        
        return response()->json($producto);
        
    }

    public function listaGuantes(Request $request)
    {
        if($request->ajax())
        {
            $producto = Producto::where('categoria', 'Guantes')->get();
            return Datatables::of($producto)->addIndexColumn()
            ->addColumn('action', function($producto){
                $button = '<button id="'.$producto->id.'" class="btn btn-success editable" type="button" data-bs-target="#modal_edito" data-bs-toggle="modal">Editar&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path><line x1="16" y1="5" x2="19" y2="8"></line></svg></button>';
                $button.= '<button id="'.$producto->id.'" class="btn btn-danger delete" type="button" style="background: var(--bs-danger);" data-bs-target="#modal_delete" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-trash-fill fs-4 text-center"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path></svg>&nbsp;Eliminar</button>';
                return $button;
            })
            ->make(true);
        }

        return view('willmaca.admin.lista_guantes');
    }

/*     public function listaProductGuantes()
    {
            
    }        */

    public function todo(Request $request)
    {
        if($request->ajax())
        {
            $producto = Producto::all();
            return Datatables::of($producto)->addIndexColumn()
            ->addColumn('action', function($producto){
                $button = '<button id="'.$producto->id.'" class="btn btn-success editable" type="button" data-bs-target="#modal_edit" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path><line x1="16" y1="5" x2="19" y2="8"></line></svg></button>';
                $button.= '<button id="'.$producto->id.'" class="btn btn-danger delete" type="button" style="background: var(--bs-danger);" data-bs-target="#modal_delete" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-trash-fill fs-4 text-center"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path></svg></button>';
                return $button;
            })
            ->make(true);
        }

        $categoria = Categoria::all();

        return view('willmaca.admin.todo', compact("categoria"));
    }
    
    public function listaLentes(Request $request)
    {
        if($request->ajax())
            {
                $producto = Producto::where('categoria', 'Lentes')->get();
                return Datatables::of($producto)->addIndexColumn()
                ->addColumn('action', function($producto){
                    $button = '<button id="'.$producto->id.'" class="btn btn-success editable" type="button" data-bs-target="#modal_edito" data-bs-toggle="modal">Editar&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path><line x1="16" y1="5" x2="19" y2="8"></line></svg></button>';
                    $button.= '<button id="'.$producto->id.'" class="btn btn-danger delete" type="button" style="background: var(--bs-danger);" data-bs-target="#modal_delete" data-bs-toggle="modal"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" class="bi bi-trash-fill fs-4 text-center"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path></svg>&nbsp;Eliminar</button>';
                    return $button;
                })
                ->make(true);
            }
       
        return view('willmaca.admin.lista_lentes');
    }

    public function vistaBotas()
    {
        $producto = Producto::where('categoria', 'Botas')->get();
        $categoria = Categoria::where('nombre', 'Botas')->get();
        return view('willmaca.admin.botas', compact('producto', 'categoria'));
    }

    public function vistaGuantes()
    {
        $producto = Producto::where('categoria', 'Guantes')->get();
        $categoria = Categoria::where('nombre', 'Guantes')->get();
        return view('willmaca.admin.guantes', compact('producto', 'categoria'));
    }

    public function vistaTapaBocas()
    {
        $producto = Producto::where('categoria', 'Tapa Bocas')->get();
        $categoria = Categoria::where('nombre', 'Tapa Bocas')->get();
        return view('willmaca.admin.tapa_bocas', compact('producto', 'categoria'));
    }

    public function vistaLentes()
    {
        $producto = Producto::where('categoria', 'Lentes')->get();
        $categoria = Categoria::where('nombre', 'Lentes')->get();
        return view('willmaca.admin.lentes', compact('producto', 'categoria'));
    }

    public function vistaTrajes()
    {
        $producto = Producto::where('categoria', 'Trajes')->get();
        $categoria = Categoria::where('nombre', 'Trajes')->get();
        return view('willmaca.admin.trajes', compact('producto', 'categoria'));
    }

    public function pruebaVue()
    {
        return view('willmaca.admin.pruebaVue');
    }

    public function productosVue()
    {
        $productos = Producto::all();
        return response()->json($productos);
    }

    public function destroy(Producto $producto, Request $request)
    {
        $producto = Producto::find($request->input("id"));
        $producto->delete();
        return response()->json(["resultado" => $producto]);
    }

    public function edit(Producto $buscar, $id)
    {
        $buscar = Producto::find($id);
        return response()->json(["resultado" => $buscar]);
    }

    public function update(Request $request, Producto $producto)
    {
        $producto = Producto::find($request->id);
        $producto->nombre = $request->input("nombre");
        $producto->precio = $request->input("precio");
        $producto->categoria = $request->input("categoria");
        $producto->descripcion = $request->input("descripcion");
        $producto->save();

        return response()->json(["resultado" => $producto]);
        
    }

}
